﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewFoto.aspx.cs" Inherits="_005ListControls.Task4.ViewFoto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        * {
            margin: 0;
            padding: 0;
            }

            body, html {
                height: 100%;
            }

        .container {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
       <asp:Literal ID="literal" runat="server" ClientIDMode="AutoID"></asp:Literal>
    </div>
    </form>
</body>
</html>
