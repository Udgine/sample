﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFoto.aspx.cs" Inherits="_005ListControls.Task4.AddFoto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 252px;
        }
        .auto-style3 {
            width: 73px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <asp:FileUpload ID="FileUploader" runat="server" Width="250px"  />
                </td>
                <td class="auto-style3">
                <asp:Button ID="buttonUpload" Text="Upload" runat="server" OnClick="buttonUpload_Click" />
                </td>
                <td>
                    <asp:Label ID="lable" runat="server" Text=""></asp:Label>
                </td>
                <td><a href="ViewFoto.aspx">View Fotos</a></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
