﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _005ListControls.Task4
{
    public partial class AddFoto : System.Web.UI.Page
    {
        List<string> _extensions = new List<string>() { ".gif", ".png", ".jpg" };
        int _maxLength = 10485760;//10*1024*1024
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string fileExtension = Path.GetExtension(FileUploader.PostedFile.FileName);

                int fileLength = FileUploader.PostedFile.ContentLength;

                if (!_extensions.Contains(fileExtension))
                {
                    throw new Exception("Wrong Extension");
                }
                if (fileLength > _maxLength)
                {
                    throw new Exception("Too heavy file");
                }

                string newFileName = Guid.NewGuid().ToString();
                string newPathForSave = Path.Combine(Server.MapPath("Uploads"), newFileName + fileExtension);

                FileUploader.SaveAs(newPathForSave);
                lable.Text = "Upload complete";
            }
            catch (Exception ex)
            {
                lable.Text = ex.Message;
            }
            
        }
    }
}